# Tobbacco_User_Registery_Network_Public

The first commit is for the script used to generate the results in our paper "Building a Tobacco User Registry by Extracting Multiple Smoking Behaviors from Clinical Notes", currently submitted to BMC Medical Informatics and Decision Making.

Future pushes will be for validated code used in moving this project forward.

The i2b2 results can be validated with access to the data.  To request access, please go to: https://www.i2b2.org/NLP/DataSets/Main.php.  The Dartmouth-Hitchcock results can only be validated on site, with IRB approval.  If you would like to do so, pelase contact Elle Palmer at ellelpalmer2018@gmail.com to start the process.

One you have the i2b2 data, you can follow the comments in the code for more detailed instructions.  
In short, have the script in a folder with three subfolders - one subfolder should contain the training data in an xml, a subfolder for results, and a subfolder with the test data in an xml. To run the script, please enter this in your command prompt:
python TURN.py [training file path] [where you want results] [test file path]

Please note this script was used with Python 2.7.12. Updates to Python 3 are forthcoming.
