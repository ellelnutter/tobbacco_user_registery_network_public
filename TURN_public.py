### Tobacco User Registery Network (TURN) code for identifying complete smoking behavior history
### Created by Elle (Nutter) Palmer
### Last code edit 2/16/2018
### Comments for additonal users added 4/23/2019

### Load all dependencies
import sys,random,os,math,nltk.data,re,string,warnings,timestring
import numpy as np
import sklearn.multiclass as multi
import xml.etree.ElementTree as x
import matplotlib.pyplot as plt
import pandas as pd
from astropy.table import Table, Column
from functools import partial
from sklearn.pipeline import Pipeline
from sklearn import svm
from sklearn.feature_extraction import text
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn import metrics
from sklearn.cross_validation import KFold
from sklearn.metrics import confusion_matrix
from collections import Counter
from datetime import datetime, timedelta, date

### designate global variables for regular expression
allowedWords = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen",
	"fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty","thirty","forty","fifty","sixty",
	"seventy","eighty","ninety","hundred","thousand","million"]
fractions = ["1/2","1/4","3/4","1/3","2/3"]
decimalsCon = [0.5,0.25,0.75,0.33,0.66] 

packWords = ["pak", "paks", "pack", "packs", "ppd", "pk", "pks","packs/day"]
yearWords = ["year", "yr", "yrs","pack/years","pk/yrs"]
counselWords = ["counsel","attempt","councel","advised","advise"]

formerWords = ["former","stopped","past","previous","distant","distal","remote","d/c'd","quit", "quitter","ex","ex-smoker","smoke free"]
currentWords = ["habits include","current","currently","continues","dependence","+","active","positive","continues to","has been","cessation","chronic","many years","is a","longstanding","interested in quit",]
neverWords = ["never","non","not","no","negative","doesn't","denies", "denied","nonsmoker","non-smoker"]

# key words for SVM hotspot identification
KEYWORDS_ss = ["smok", "cig", "nicoti","tobac"] # for status
KEYWORDS_qd = ["quit", "qd","stop"] # for quit date
KEYWORDS_sh = ["cig", "smok","pk", "ppd","pack", "pak", "pkyr"] # for history

STOP_WORDS = text.ENGLISH_STOP_WORDS - set(["neither", "never", "no", "nor", "not"]) # delete negation from stop words list

### outcomes we will classify records in to
### set is slightly different between sources - have commented out the DH set
#DESIRED_CLASSES_SS = set(["never_smoker","current_smoker","past_smoker","smoker","unknown"]) # this is the set for DH records
DESIRED_CLASSES_SS = set(["non_smoker","current_smoker","past_smoker","smoker","unknown","ever_smoker"]) # this set is for i2b2 wording

SENT_DETECTOR = nltk.data.load('tokenizers/punkt/english.pickle') # load dictionary that recognizes words
printable = set(string.printable)

# indicate the testing and training sets for smoking status algorithm
train_corpus_ss = []
test_corpus_ss = []
train_corpus_sh = []
train_corpus_qd = []
train_labels_ss = []
test_labels_ss = []
train_labels_sh = []
train_labels_qd = []
record_date = []
quit_split = [] # empty vector to get quit dates.
 
def create_hotspots(document, hotspot_roots):
	# ensure variable is empty
	sentences = None
    # split text in to sentences
	try:
		sentences = SENT_DETECTOR.tokenize(document.strip())
	except UnicodeDecodeError:
		sentences = SENT_DETECTOR.tokenize(document.decode('utf-8').strip())
	# collect visit date as we are decomposing the record to hotspots
	visit_date = None # ensure it starts out blank in case of an error
	get_date = sentences[0] # we know which sentence the date is in for i2b2 records, so get that!
	match=re.search(r'(\d+/\d+/\d+)',get_date) # we know the format visit dates are put in, so we can just match it
	visit_date = timestring.Date(match.group(1)).date # format date
	record_date.append(visit_date) # return date

	hotspots = []
	for key in hotspot_roots:
		for j,sentence in enumerate(sentences):
			words = sentence.split()
			for word in words:
				if key in word.lower():
					spot = words.index(word)
					lower = max(spot-9,0)
					upper = min(spot+9,len(words))
					wordspot = words[lower:upper]
					wordspot2 = ' '.join(wordspot)
					hotspots.append(wordspot2)
					words = []
					break
	if hotspots == []: # send back to pre_populate_training_set the hotspots with a simple string if no words found
		hotspots.append("no_info")
	return ';'.join(set(hotspots))

def create_hotspots_2(document, hotspot_roots):
	# ensure variable is empty
    sentences = None
    # split text in to sentences
    try:
        sentences = SENT_DETECTOR.tokenize(document.strip())
    except UnicodeDecodeError:
        sentences = SENT_DETECTOR.tokenize(document.decode('utf-8').strip())
    hotspots = []
	#create a list of the hotspots in the text to run further analyses on
    for key in hotspot_roots:
        for j,sentence in enumerate(sentences):
            words = sentence.split()
            for word in words:
                if key in word.lower():
                    spot = words.index(word)
                    lower = max(spot-5,0)
                    upper = min(spot+5,len(words))
                    wordspot = words[lower:upper]
                    wordspot2 = ' '.join(wordspot)
                    hotspots.append(wordspot2)
                    break

				#hotspots.append(s)
	# send back to pre_populate_training_set the hotspots
	if hotspots == []:
		hotspots.append("starter")
    return ';'.join(set(hotspots))

	# ensure variable is empty
    sentences = None
    # split text in to sentences
    try:
        sentences = SENT_DETECTOR.tokenize(document.strip())
    except UnicodeDecodeError:
        sentences = SENT_DETECTOR.tokenize(document.decode('utf-8').strip())

    hotspots = []
	#create a list of the hotspots in the text to run further analyses on
    for key in hotspot_roots:
        for j,sentence in enumerate(sentences):
            words = sentence.split()
            for word in words:
                if key in word.lower():
                    spot = words.index(word)
                    lower = max(spot-5,0)
                    upper = min(spot+5,len(words))
                    wordspot = words[lower:upper]
                    wordspot2 = ' '.join(wordspot)
                    hotspots.append(wordspot2)
                    break

				#hotspots.append(s)
    if hotspots == []: 	# send back to pre_populate_training_set the hotspots
        hotspots.append("no_info")
    return ';'.join(set(hotspots))

def pre_populate_training_set_i2b2(in_path, hotspot_roots, type, tt_label):
	# walks through all the records in the corpus
    for dname, dirs, files in os.walk(in_path):
        for fname in files:
            if fname.endswith('.xml'):
				#get a single record
				root = x.parse(os.path.join(dname, fname)).getroot()
				for record in root.findall('RECORD'):
					recordID = record.get('ID')
					# ensure variables associated with the record are empty
					hotspots = None
					classifier = None
					classifier2 = "no status loop"
                    # look at the record in question
					for child in record: # reports only one smoking status and text per record
						tag = child.tag
						# loop through the layers
						if tag == 'SMOKING': 
							classifier = child.get('STATUS').lower().replace('-','_').replace(' ','_')
							classifier2 = "in status"
						# read the text record, pull sentences related to smoking concepts
						if tag == 'TEXT':
							# pull full sentence related to concepts of interest
							if type == "ss":
								hotspots = create_hotspots_2(child.text, hotspot_roots)
								#if classifier == "current_smoker" or classifier == "past_smoker" or classifier == "smoker":
								#	classifier = "ever_smoker"
							else:
								hotspots = create_hotspots(child.text, hotspot_roots)
							if classifier == None:
								classifier = "unknown"
					# Check that we actually have data to use, and then append it to the corpus!
					if hotspots and len(hotspots) and classifier in DESIRED_CLASSES_SS:
						assert (classifier is not None and classifier in DESIRED_CLASSES_SS)
						if type == "ss":
							if tt_label == "train":
								train_corpus_ss.append(hotspots)
								train_labels_ss.append(classifier)
							if tt_label == "test":
								test_corpus_ss.append(hotspots)	
								test_labels_ss.append(classifier)
						if type == "sh":
							train_corpus_sh.append(hotspots)
						if type == "qd":
							train_corpus_qd.append(hotspots)
						
	
def pre_populate_training_set(in_path, hotspot_roots, type):
    pattern = re.compile('CONTACT_DATE : (.*) 00:00:00.0000000')
    for dname, dirs, files in os.walk(in_path):
        for fname in files:
            if fname.endswith('.xml'):
				#get a single record
				root = x.parse(os.path.join(dname, fname)).getroot()
				for record in root.findall('RECORD'):				
					# ensure it starts out blank in case of an error
					# ensure variables associated with the record are empty
					hotspots = None
					visit_date = None
					ssclassifier = "unknown" # default setting
					qdclassifier = "no_quit_date" # default setting
					shclassifier = "no_history" # default setting
					classifier2 = "no status loop"
					# look at the record in question
					for child in record: # one smoking status and text per record
						tag = child.tag		
						if tag == 'DETAILS':
							get_date = child.text.strip()
							match = pattern.findall(get_date)
							visit_date = timestring.Date(match).date # format date
							record_date.append(visit_date) # return date
							
						if tag == 'classMention': # loop through the layers
							for subject in child:                         
								tag2 = subject.tag        				
								if tag2 == ('mentionClass'): # loop through the layers
									# get smoking status true label from record
									if subject.get('STATUS') != None:
										ssclassifier = subject.get('STATUS').lower().replace('-','_').replace(' ','_')
									# get quit date true label from record
									# check to see if there should be a label - never smokers, current smokers, smoker, and unknown should automatically be no label
									if subject.get('QUIT') != None:
										qdclassifier = subject.get('QUIT').lower().replace('-','_').replace(' ','_')
										if qdclassifier != "quit_date":
											qdclassifier = "no_quit_date"
									#if qdclassifier == None:
									#	if ssclassifier == "never_smoker" or ssclassifier == "current_smoker" or ssclassifier == "smoker" or ssclassifier == "unknown":
									#		qdclassifier = "no_quit_date"
									# get smoking history true label from record
									# check for never smoker or unknown - true label is no information
									if shclassifier == None:
										if ssclassifier == "never_smoker" or ssclassifier == "unknown":
											shclassifier = "no_history"
										elif ssclassifier == "current_smoker" or ssclassifier == "past_smoker" or ssclassifier == "smoker": 
											shclassifier = "no_history"
									if subject.get('HISTORY') == "Amount_length_used":
										shclassifier = "Amount_length_used"
						# read the text record, pull sentences related to smoking concepts
						if tag == 'TEXT':
							if type == "ss":
								hotspots = create_hotspots_2(child.text, hotspot_roots) # Pull only +/- 5 words for SVM
							else:
								hotspots = create_hotspots(child.text, hotspot_roots) # Pull more liberally for regex matching
							if ssclassifier == None:
								ssclassifier = "unknown"
					# Check that we actually have data to use, and then append it to the corpus!
					#print ssclassifier, shclassifier, qdclassifier
					if hotspots and len(hotspots) and ssclassifier in DESIRED_CLASSES_SS:
						assert (ssclassifier is not None and ssclassifier in DESIRED_CLASSES_SS)
						if type == "ss":
							train_corpus_ss.append(hotspots)
						if type == "sh":
							train_corpus_sh.append(hotspots)
						if type == "qd":
							train_corpus_qd.append(hotspots)
						train_labels_ss.append(ssclassifier)
						train_labels_sh.append(shclassifier)
						train_labels_qd.append(qdclassifier)

def rfn(text_string): # return float as number
	wtn = WordsToNumbers()
	noi = "NA"
	text_string = [text_string[0]]
	text_string = [words for segments in text_string for words in segments.split()]
	for i,element in enumerate(text_string):
		# check elements of text string for a float number
		try:
			if element == float(element):
				continue		
			noi = float(element) # noi = number of interest
			break
		# if not a number, then check string for a text number
		except:
			# if number words found, put them in a format to read in to wtn
			if element in allowedWords:
				text_string[i] = text_string[i]
			else:
				text_string[i] = ""
	# if noi is NA, then test for word numbers using text_string sent to wtn function
	if noi == "NA":
		text_string = re.sub("\s\s+", " ", ' '.join(text_string).strip())
		try:
			noi = wtn.parse(text_string)
		except KeyError:
			# if neither a floating number or text number, return NA
			noi = "NA"
		else:
			noi = wtn.parse(text_string)
	return(noi)
	
### WordstoNumbers copied from: http://code.activestate.com/recipes/550818-words-to-numbers-english/
### in October 2017
class WordsToNumbers():
	"""A class that can translate strings of common English words that
	describe a number into the number described
	"""
	# a mapping of digits to their names when they appear in the
	# relative "ones" place (this list includes the 'teens' because
	# they are an odd case where numbers that might otherwise be called
	# 'ten one', 'ten two', etc. actually have their own names as single
	# digits do)
	__ones__ = { 'one': 1, 'eleven': 11,'two': 2, 'twelve': 12,
				'three': 3, 'thirteen': 13,'four': 4, 'fourteen': 14,
				'five': 5, 'fifteen': 15,'six': 6, 'sixteen': 16,
				'seven': 7, 'seventeen': 17,'eight': 8, 'eighteen': 18,
				'nine': 9, 'nineteen': 19,'zero': 0}
	# a mapping of digits to their names when they appear in the 'tens'
	# place within a number group
	__tens__ = { 'ten': 10,'twenty': 20,'thirty': 30,
				'forty': 40,'fifty': 50,'sixty': 60,
				'seventy': 70,'eighty': 80,'ninety': 90}
    # an ordered list of the names assigned to number groups
	__groups__ = { 'thousand': 1000, 'million': 1000000, 'billion': 1000000000,'trillion': 1000000000000 }
	# a regular expression that looks for number group names and captures:
	#     1-the string that preceeds the group name, and
	#     2-the group name (or an empty string if the
	#       captured value is simply the end of the string
	#       indicating the 'ones' group, which is typically
	#       not expressed)
	__groups_re__ = re.compile(
		r'\s?([\w\s]+?)(?:\s((?:%s))|$)' %
		('|'.join(__groups__))
		)
	# a regular expression that looks within a single number group for
	# 'n hundred' and captures:
	#    1-the string that preceeds the 'hundred', and
	#    2-the string that follows the 'hundred' which can
	#      be considered to be the number indicating the
	#      group's tens- and ones-place value
	__hundreds_re__ = re.compile(r'([\w\s]+)\shundred(?:\s(.*)|$)')
	# a regular expression that looks within a single number
	# group that has already had its 'hundreds' value extracted
	# for a 'tens ones' pattern (ie. 'forty two') and captures:
	#    1-the tens
	#    2-the ones
	__tens_and_ones_re__ =  re.compile(
		r'((?:%s))(?:\s(.*)|$)' %
		('|'.join(__tens__.keys()))
		)
	def parse(self, words):
		"""Parses words to the number they describe"""
		# to avoid case mismatch, everything is reduced to the lower
		# case
		words = words.lower()
		# create a list to hold the number groups as we find them within
		# the word string
		groups = {}
		# create the variable to hold the number that shall eventually
		# return to the caller
		num = 0
		# using the 'groups' expression, find all of the number group
		# an loop through them
		for group in WordsToNumbers.__groups_re__.findall(words):
		## determine the position of this number group
			## within the entire number
			# assume that the group index is the first/ones group
			# until it is determined that it's a higher group
			group_multiplier = 1
			if group[1] in WordsToNumbers.__groups__:
				group_multiplier = WordsToNumbers.__groups__[group[1]]
			## determine the value of this number group
			# create the variable to hold this number group's value
			group_num = 0
			# get the hundreds for this group
			hundreds_match = WordsToNumbers.__hundreds_re__.match(group[0])
			# and create a variable to hold what's left when the
			# "hundreds" are removed (ie. the tens- and ones-place values)
			tens_and_ones = None
			# if there is a string in this group matching the 'n hundred'
			# pattern
			if hundreds_match is not None and hundreds_match.group(1) is not None:
				# multiply the 'n' value by 100 and increment this group's
				# running tally
				group_num = group_num + \
							(WordsToNumbers.__ones__[hundreds_match.group(1)] * 100)
				# the tens- and ones-place value is whatever is left
				tens_and_ones = hundreds_match.group(2)
			else:
			# if there was no string matching the 'n hundred' pattern,
			# assume that the entire string contains only tens- and ones-
			# place values
				tens_and_ones = group[0]
			# if the 'tens and ones' string is empty, it is time to
			# move along to the next group
			if tens_and_ones is None:
				# increment the total number by the current group number, times
				# its multiplier
				num = num + (group_num * group_multiplier)
				continue
			# look for the tens and ones ('tn1' to shorten the code a bit)
			tn1_match = WordsToNumbers.__tens_and_ones_re__.match(tens_and_ones)
			# if the pattern is matched, there is a 'tens' place value
			if tn1_match is not None:
				# add the tens
				group_num = group_num + WordsToNumbers.__tens__[tn1_match.group(1)]
				# add the ones
				if tn1_match.group(2) is not None:
					group_num = group_num + WordsToNumbers.__ones__[tn1_match.group(2)]
			else:
			# assume that the 'tens and ones' actually contained only the ones-
			# place values
				group_num = group_num + WordsToNumbers.__ones__[tens_and_ones]
			# increment the total number by the current group number, times
			# its multiplier
			num = num + (group_num * group_multiplier)
		# the loop is complete, return the result
		return num            		
	
### This function will take the vector of strings with potential pack year information and converts it
### in to a single PY floating number per record / string.
### handles numbers and word numbers, cigarettes/day, packs/day, and PY formats

def Split_ppd_yrs(nums, pred_ss):
	print len(nums)
	# empty vectors to return all smoking history related values
	pyret = [0]*len(nums); cigret = [0]*len(nums); ppdret = [0]*len(nums); ysret = [0]*len(nums); 
	amlInfo = []
	# for each word in the word string identified as having pack year history
	for i in range(0,len(nums)):
		# zero local variables called in each record review
		if pred_ss[i] == "unknown" or pred_ss[i] == "non_smoker" or pred_ss[i] == "never_smoker":
			cpd = ppd = ys = py2 = info = "NA"
		else:
			cpd = ppd = ys = py2 = info = "NA"
			# zero vectors for each new record
			CPD = PPD = PY = CYS = PYS = []
			# zero counter
			j = 0; fraction_add = 0
			# get string in to working format
			num = re.sub('[+,:,;,-,-,x,(,)]', ' ', nums[i]).lower(); num = num.split()#status = re.sub('[,.!?;]', '', classStatus[i])u'\ufffd'
			# set up regular expression patterns
			cigs = r'((?:\w*\s*){3})\s*cig' # gets CPD
			prePack = r'((?:\w*\s*){3})\s*pack' # gets PPD
			pYears = r'((?:\w*\s*){3})\s*year' # gets a single number pack years
			pYears2 =r'((?:\w*\s*){3})pyr' 
			cYears = r'cig(.*?)year' # gets a number that needs to be multiplied by CPD or PPD
			cYears2 = r'pack(.*?)year' 
			### need to handle pack years and smoke years seperately to prevent issues...
			# find words that are equivalent and set up string to take regular expression patterns
			for j,word in enumerate(num):
				m = len(num)
				# code in for other fractions?
				if m > (j+1):
					if word == "a" and num[j+1] == "pack":
						num[j] = "1"
				if word == "1/2" or word == "half":
					num[j] = "0.5"
				if word == "1/4" or word == "quarter":
					num[j] = "0.25"
				# check for cigar - we don't want to count as cigarette smoking
				if "cig" in word:
					if word == "cigar": # rule to ignore cigar information
						num[j] == "OTB" # other tobacco product, means we don't take cigar info
					else:
						num[j] = "cig" # simplifies all cigarrette smoking terms to "cig"
				for string in packWords:
					if string in word.lower():	
						if word.lower == "packed":
							num[j] = "nonsense"
						else:
							num[j] = "pack" # simplify all pack terms to "pack"
				for string in yearWords:
					if string in word.lower():
						if m > j:
							if (j+1) < len(num) and num[j+1] == "old": # rule to exclude "year old" wording
								num[j] = "yr"
							else:
								num[j] = "year" # simplify all other year terms to "year"
			### create the 3 possible regular expression vectors to find exact numbers
			try:
				num = ' '.join(map(str, num)) # put in to a re searchable string
				#sentences = SENT_DETECTOR.tokenize(document.strip())
			except UnicodeEncodeError:
				temp = [s.encode('ascii', 'ignore') for s in num]
				num = ' '.join(map(str, temp))
			CPD = re.findall(cigs,num) # cig per day term (in case we need to do math)
			PPD = re.findall(prePack,num) # pack per day term (in case we need to do math)
			CYS = re.findall(cYears, num) # cigarettes per year term (for math only)
			PY = re.findall(cYears2, num) # packs per year term (for math only)
			PYS = re.findall(pYears, num) # Pack years without need for math
			if PYS == []:
				PYS = re.findall(pYears2, num) # alternate form we could find PY with
					
			# assume we have nothing, and fill in all blanks
			if PY == [] and PPD == [] and CPD == [] and CYS == [] and PYS == []:
				cpd = ppd = ys = py2 = "NA"
				info = "no_history"
			else: # we know we have something!
				info = "Amount_length_used"
				# assumes partial data -> we only have CPD or PPD
				if PY == [] and CYS == [] and PYS == []:
					ys = py2 = "NA"
					if CPD != [] and PPD == []:
						cpd = rfn(CPD) # only have partial data
					if PPD != []: # pack data overrules cig data
						ppd = rfn(PPD)
						if ppd != "NA" and ppd > 0:
							ppd = ppd + fraction_add # assume full data?
						if ppd > 5: # assume <5 refers to PPD, anything greater PY
							py2 = ppd	
				# assumes we can construct PY from CPD or PPD + YS
				if CYS !=[] or PYS != []:
					# we look for CPD and YS to calculate PY
					if CPD != []:
						ys = rfn(CYS)
						cpd = rfn(CPD)	
						if cpd != "NA" and ys != "NA" and cpd <10:
							py2 = (cpd*ys)/20
						if cpd != "NA" and cpd > 10:
							py2 = cpd
					# we look for PPD and YS to calculate years
					if PPD != []: # pack data overrules cig data
						ys = rfn(PYS)
						ppd = rfn(PPD)
						if ppd != "NA" and ppd > 0:
							ppd = float(ppd)+float(fraction_add)
						if ppd != "NA" and ys != "NA" and ppd != 0 and ys != 0 and ppd <5:
							py2 = ppd*ys
						else:
							if (ys == 0 or ys == "NA" or ys == ppd) and ppd > 5: # assumes 1-4 refers to PPD, anything greater PY
								py2 = ys; ppd = "NA"
							if ys != "NA" and (ppd == 0 or ppd == "NA"):
								py2 = "check text"
				if PY != [] and py2 =="NA":
					py2 = rfn(PY)
				# assumes we have a numeric pack year, record other information for reflection
				if ys > 250 and ys !="NA":
					ys = "NA"
				if py2 > 250 and py2 != "NA":
					py2 = "NA"
		cigret[i] = cpd; ppdret[i] = ppd; ysret[i] = ys; pyret[i] = py2
		amlInfo.append(info)
	return(cigret, ppdret, ysret, pyret,amlInfo)

### classify_status is not reported in our BMC Medical Informatics and Decison Making paper due to lower performance than the SVM.  
### However, it may be useful for future projects and is left for readers to utilize with citation.
def classify_status(classStatus):
	statRet = []
	for i in range(0,len(classStatus)):
		# zero local variables called in each record review
		current = former = smoker = never = unknown = ever = "No"; Pstatus = ""
		# get record in to working form (each word element of list)
		status = re.sub('[,!?]', '', classStatus[i])
		generic = r'do not smoke'; generic2 = r'Do not smoke'; generic3 = r"Don't smoke"; generic4 = r"don't smoke" # finds generic advise
		status = re.sub(generic, 'starter', status); status = re.sub(generic2, 'starter', status); status = re.sub(generic3, 'starter', status); status = re.sub(generic4, 'starter', status)
		### check if word is in a list of indicator words
		### if yes, change indicator to yes
		words = status.lower()
		words = words.split()
		for word in words:
			for counsel in counselWords:
				if counsel in word.lower(): # if counselled, we know current smoker...
					current = "Yes"
			for key in currentWords: # check for current word
				if key in word.lower():
					current = "Yes"
			for key in KEYWORDS_ss: # check for smoker words
				if key in word.lower():
					smoker = "Yes"
			for key in formerWords: # check for former words
				if key in word.lower():
					former = "Yes"
			for key in neverWords: # check for never words
				if key == word.lower():
					never = "Yes"
		### if not in any list, change unknown indicator to yes
		if current == former == never == smoker == "No":
			unknown = "Yes"
		### put the indicators in to ordered vector, and pair with final field label
		statusVec = [unknown, smoker, never, former, current, ever]
		# for DH
		#finalStat = ["unknown","smoker","never_smoker","past_smoker","current_smoker"]
		# for i2b2
		finalStat = ["unknown","smoker","non_smoker","past_smoker","current_smoker"]
		### loop through taking the highest given label as final label
		for i in range(0,6):
			if statusVec[i] == "Yes":
				pStatus = finalStat[i]
		print(pStatus)
		#if pStatus == "past_smoker" or pStatus == "current_smoker" or pStatus == "smoker":
		#	pStatus = "ever_smoker"
		statRet.append(pStatus)#print Counter(statRet)
	return(statRet)	

def find_qd(nums):
	i = 0 # empty counter
	math_needed = None # set as empty variable that tells us if we have to calculate QD from "years ago"
	dateFound = []
	for Val in nums:
		dateF = "quit_date"
		# this will reduce the possible number of false positives, since many dates exist in EHR records
		# ignore records classified as unknown, or ones that have no corpus identified
		temp = None # ensure every record starts out with a blank temp variable to collect quit date
		Val = filter(lambda x: x in printable, Val)
		if predicted_labels[i] == "unknown" or predicted_labels[i] == "never_smoker" or predicted_labels[i] == "non_smoker" or Val == "starter":
			temp = None; dateF = "no_quit_date"
		else:
			t = timestring.Date(record_date[i]).date; today = datetime.combine(t, datetime.min.time()) # set today to visit date for record
			math_needed = re.search(r'ago',Val) # determine if the word "ago is present; if present, need to take today - yrs since quit
			Val = Val.lower().split(); Val = ' '.join(map(str, Val))
			Val = re.split('\\b(quit|stop|qd)\\b',Val)[-1] # gets PPD #((?:\w*\s*){5})
			max_qd = today - timedelta(days=18250) # setting max quit date as 50 years prior to visit to prevent errors
			try:
				temp = timestring.Date(Val).date # see if it catches a date with built in functions
			except timestring.TimestringInvalid: # if not, then assume no date at this point... (will build in rules here)
				temp = None; dateF = "no_quit_date"
			except ValueError:
				temp = None; dateF = "no_quit_date"
			### if it had a "ago" wording, do time delta because we need to adjust for visit date.
			if math_needed != None and temp != None:
				date =  datetime.today() # get today's date
				adjust =  (datetime.today() - today).days # subtract the actual visit day from today
				temp2 = datetime.combine(temp, datetime.min.time()) # get temp in to the correct format
				temp = temp2 - timedelta(days=adjust) # subtract time between today and visit date from temp2 to get actual quit date
			if temp != None:
				if temp >= record_date[i] or temp< max_qd: # record date comes from hotspots as a seperate vector
					temp = None; dateF = "no_quit_date"
			if temp == None:
				dateF = "no_quit_date"
		i += 1
		quit_split.append(temp)
		dateFound.append(dateF)
	return(quit_split,dateFound)

### Create the matrix showing classifications (vizual aide to performance)
def print_cm(cm, labels, hide_zeroes=False, hide_diagonal=False, hide_threshold=None):
    print("print_cm")
    """pretty print for confusion matrixes"""
    columnwidth = max([len(x) for x in labels]+[5]) # 5 is value length
    empty_cell = " " * columnwidth
    # Print header
    print "    " + empty_cell,
    for label in labels: 
        print "%{0}s".format(columnwidth) % label,
    print
    # Print rows
    for i, label1 in enumerate(labels):
        print "    %{0}s".format(columnwidth) % label1,
        for j in range(len(labels)): 
            cell = "%{0}.2f".format(columnwidth) % cm[i, j]
            if hide_zeroes:
                cell = cell if float(cm[i, j]) != 0 else empty_cell
            if hide_diagonal:
                cell = cell if i != j else empty_cell
            if hide_threshold:
                cell = cell if cm[i, j] > hide_threshold else empty_cell
            print cell,
        print

def plot_confusion_matrix(cm, labels, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation=45)
    plt.yticks(tick_marks, labels)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

### This function shows which regular experession labels are correct in a confusion matrix
def decide(train_labels,regexp_labels):
	
    print "Micro prec\t",metrics.precision_score(train_labels, regexp_labels, average='micro')
    print "Micro recall\t",metrics.recall_score(train_labels, regexp_labels, average='micro') 
    print "Micro f1\t",metrics.f1_score(train_labels, regexp_labels, average='micro'),'\n'
    print metrics.classification_report(train_labels, regexp_labels)
    
    axes = list(set(train_labels))
    cm = confusion_matrix(train_labels, regexp_labels, labels=axes)

    # Compute confusion matrix
    np.set_printoptions(precision=2)
    print('Confusion matrix, without normalization')
    print_cm(cm,axes)
    plt.figure()
    plot_confusion_matrix(cm, axes)

    # Normalize the confusion matrix by row (i.e by the number of samples
    # in each class)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print('Normalized confusion matrix')
    print_cm(cm_normalized,axes)
    plt.figure()
    plot_confusion_matrix(cm_normalized, axes, title='Normalized confusion matrix')

    plt.show()
	
### This function runs the SVM and reports the summary statistics in a confusion matrix
def decide2(train_corpus,train_labels,test_corpus,test_labels):
	print("in decide")
	clf = svm.SVC(kernel='linear', probability=True, class_weight='balanced')
	pipeline = Pipeline([('vectorizer', text.TfidfVectorizer(stop_words=None)),('clf', clf)])  
	pipeline.fit(train_corpus, train_labels)
	
	predicted = pipeline.predict(test_corpus)
	predicted_proba = pipeline.predict_proba(test_corpus)

	print "Micro prec\t",metrics.precision_score(test_labels, predicted, average='micro')
	print "Micro recall\t",metrics.recall_score(test_labels, predicted, average='micro') 
	print "Micro f1\t",metrics.f1_score(test_labels, predicted, average='micro'),'\n'
	print metrics.classification_report(test_labels, predicted)
	
	axes = list(set(test_labels))
	cm = confusion_matrix(test_labels, predicted, labels=axes)

    # Compute confusion matrix
	np.set_printoptions(precision=2)
	print('Confusion matrix, without normalization')
	print_cm(cm,axes)
	plt.figure()
	plot_confusion_matrix(cm, axes)

    # Normalize the confusion matrix by row (i.e by the number of samples
    # in each class)
	cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
	print('Normalized confusion matrix')
	print_cm(cm_normalized,axes)
	plt.figure()
	plot_confusion_matrix(cm_normalized, axes, title='Normalized confusion matrix')

	plt.show()
	return predicted, predicted_proba, clf.classes_

##########################
### main commands here ###
##########################
    
if (sys.argv > 3):
    # give the automatic paths to in and out documents 
	# The command line should feed is: python TURN.py [training file path] [where you want results] [test file path]
	in_path = sys.argv[1]
	out_path = sys.argv[2]
	doc_path = sys.argv[3]
	############
	### i2b2 ###
	############
	### To run, put the training records in a subfolder [given as in_path] and the test records in a seperate subfolder [given as doc_path]
	### These subfolders should be in the same folder as this script
	
	### run status corpus search and regular expression extraction
	
	pre_populate_training_set_i2b2(os.path.join("./", in_path),KEYWORDS_ss,"ss", "train") # works with the .xml file in here
	pre_populate_training_set_i2b2(os.path.join("./", doc_path),KEYWORDS_ss,"ss","test") # works with the .xml file in here
	predicted_labels, predicted_proba , proba_labels = decide2(train_corpus_ss,train_labels_ss,test_corpus_ss,test_labels_ss)
	
	# run smoking history corpus search and regular expression extraction
	pre_populate_training_set_i2b2(os.path.join("./", doc_path),KEYWORDS_sh,"sh","test") # works with the .xml file in here
	cpdtot, ppdtot, yrstot, pytot,aml = Split_ppd_yrs(train_corpus_sh, predicted_labels)
	
	# run quit date corpus search and regular expression extraction
	pre_populate_training_set_i2b2(os.path.join("./", doc_path),KEYWORDS_qd,"qd","test")
	quit_split, qDate = find_qd(train_corpus_qd)
	
    #write the output file
	with open(os.path.join(out_path, "i2b2_test_final.txt"), 'w') as f:
		f.write("true status| predicted status| cpd| ppd| smkyrs| py| quit date| ss text| sh text| qd text")
		for test_labels_ss, predicted_labels, cpdtot, ppdtot, yrstot, pytot, quit_split, test_corpus_ss, train_corpus_sh, train_corpus_qd in zip(test_labels_ss, predicted_labels, cpdtot, ppdtot, yrstot, pytot, quit_split, test_corpus_ss, train_corpus_sh, train_corpus_qd):    
			try:
				str(text)
			except UnicodeEncodeError:
				text = text.encode('utf-8').strip()
			f.write('\n' + test_labels_ss + '|' + predicted_labels + '|' + str(cpdtot) + '|' + str(ppdtot) + '|' + str(yrstot) + '|' + str(pytot) + '|' + str(quit_split) + '|' +str(test_corpus_ss) + '|' + str(train_corpus_sh) + '|' + str(train_corpus_qd) +'"')

	################
	## DH record ###
	################
	# run status corpus search and regular expression extraction
	#pre_populate_training_set(os.path.join("./", in_path),KEYWORDS_ss,"ss") # works with the .xml file in here
	#pre_populate_training_set(os.path.join("./", doc_path),KEYWORDS_ss,"ss") # works with the .xml file in here
	#predicted_labels, predicted_proba , proba_labels = decide2(train_corpus_ss,train_labels_ss,test_corpus_ss,test_labels_ss)
		
	# run smoking history corpus search and regular expression extraction
	#pre_populate_training_set(os.path.join("./", in_path),KEYWORDS_sh,"sh") # works with the .xml file in here
	#pre_populate_training_set(os.path.join("./", doc_path),KEYWORDS_sh,"sh") # works with the .xml file in here
	#cpdtot, ppdtot, yrstot, pytot,aml = Split_ppd_yrs(train_corpus_sh, predicted_labels)
	#proba_labels2 = decide(train_labels_sh[0:len(aml)], aml)
	
	# run quit date corpus search and regular expression extraction
	#pre_populate_training_set(os.path.join("./", in_path),KEYWORDS_qd,"qd")
	#pre_populate_training_set(os.path.join("./", doc_path),KEYWORDS_qd,"qd")
	#quit_split, qDate = find_qd(train_corpus_qd)
	#proba_labels3 = decide(train_labels_qd[0:len(qDate)], qDate)

    #write the output file
	#with open(os.path.join(out_path, "TURN_TEST_final.txt"), 'w') as f:
	#	f.write("true status| predicted status|true sh|cpd|ppd|smkyrs|py|true qd|quit date|ss text|sh text|qd text")
	#	for train_labels_ss, predicted_labels, train_labels_sh, cpdtot, ppdtot, yrstot, pytot, train_labels_qd, quit_split, train_corpus_ss, train_corpus_sh, train_corpus_qd in zip(train_labels_ss, predicted_labels, train_labels_sh, cpdtot, ppdtot, yrstot, pytot, train_labels_qd, quit_split, train_corpus_ss, train_corpus_sh, train_corpus_qd):    
	#		try:
	#			str(text)
	#		except UnicodeEncodeError:
	#			print "exception!"
	#			text = filter(lambda x: x in printable, text)
	#		f.write('\n' + train_labels_ss + '|' + predicted_labels + '|' + str(train_labels_sh) + '|' + str(cpdtot) + '|' + str(ppdtot) + '|' + str(yrstot) + '|' + str(pytot) + '|' + str(train_labels_qd) + '|' + str(quit_split) + '|' + filter(lambda x: x in printable, train_corpus_ss) + '|' + filter(lambda x: x in printable, train_corpus_sh) + '|' + filter(lambda x: x in printable, train_corpus_qd) +'"')
